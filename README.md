# fiches_rappels_cours

fiches de rappels de mes cours. A partager mais elles n'ont pas beaucoup d'intérêts sans le cours.

Vous trouverez ci-dessous les liens vers un ensemble de "fiches de rappels". (dans les dossiers pdf)
Ces fiches retracent les grandes lignes des éléments de cours vus lors des TD.
Ce ne sont que des rappels rapides, non exhaustifs et peu détaillés, je vous conseille de lire le manuel QGIS et tous les tutos du web (dont les miens !) afin de compléter ces informations.
